/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package widget.com.worldline.interview;

import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author USER
 */
public class widget {
    public static void main(String[] args) throws IOException {
        Scanner scan = new Scanner(System.in);
        WidgetMachine wm = new WidgetMachine();
        System.out.println("Please enter fuel type");
        String fuel = scan.nextLine();
        
        System.out.println("Please enter fuel level");
        int fuellevel = scan.nextInt();
        
        FuelType ft = FuelType.valueOf(fuel);
        InternalCombustionEngine ic = new InternalCombustionEngine(ft);
        double cost = wm.produceWidgets(20, ic, ft, fuellevel);
        System.out.printf("Total Cost: $%.2f", cost);
        System.out.println();
    }
}
