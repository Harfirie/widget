package widget.com.worldline.interview;

public enum FuelType {
    PETROL, DIESEL, WOOD, COAL
}
