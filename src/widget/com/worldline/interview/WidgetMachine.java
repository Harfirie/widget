package widget.com.worldline.interview;

public class WidgetMachine {
//    private InternalCombustionEngine engine = new InternalCombustionEngine(FuelType.PETROL);

    public double produceWidgets(int quantity, InternalCombustionEngine engine, FuelType ft, int fuellevel) {
        engine.fill(ft, fuellevel);
        engine.start();
        double cost = 0;

        if (engine.isRunning()) {
            cost = produce(quantity, engine);
        }

        engine.stop();

        return cost;
    }

    private double produce(int quantity, InternalCombustionEngine engine) {
        int batch = 0;
        int batchCount = 0;
        double costPerBatch = 0;

        if (null != engine.getFuelType()) switch (engine.getFuelType()) {
            case PETROL:
                costPerBatch = 9;
                break;
            case DIESEL:
                costPerBatch = 12;
                break;
            case WOOD:
                costPerBatch = 4.35;
                break;
            case COAL:
                costPerBatch = 5.65;
                break;
            default:
                break;
        }

        while (batch < quantity) {
            batch = batch + 8;
            batchCount++;
        }

        return batchCount * costPerBatch;
    }


}
